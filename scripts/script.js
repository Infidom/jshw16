function fibonacci(firstNumber, secondNumber, number) {
    if (number === 2) return secondNumber;
    if (number === 1) return firstNumber;
    let f1 = +firstNumber;
    let f2 = +secondNumber;
    let f3;
    if (number > 0) {
        for (let i = 3; i <= number; i++) {
            f3 = f1 + f2;
            f1 = f2;
            f2 = f3;
        }
    } else if (number < 0) {
        for (let i = -3; i >= number; i--) {
            f3 = f1 - f2;
            f1 = f2;
            f2 = f3;
        }
    }
    console.log(f3);
}

let number;
let numberA;
let numberB;

do {
    numberA = prompt("Enter a number A, please", [numberA]);
    numberB = prompt("Enter a number B, please", [numberB]);
} while (
    isNaN(numberA) ||
    isNaN(numberB) ||
    numberA == null ||
    numberB == null ||
    numberA.trim().length === 0 ||
    numberB.trim().length === 0
);

do {
    number = +prompt("Enter a number, please", [number]);
    numberCheck = Number.isInteger(number);
} while (isNaN(number) || numberCheck == false);

fibonacci(numberA, numberB, number);
